# OpenML dataset: liverTox_ALT_target

https://www.openml.org/d/46133

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data come from a liver toxicity study in which 64 male rats were exposed to non-toxic (50 or 150 mg/kg),moderately toxic (1500 mg/kg) or severely toxic (2000 mg/kg) doses of acetaminophen (paracetamol) (Bushel, Wolfinger, and Gibson 2007).Necropsy was performed at 6, 18, 24 and 48 hours after exposure and the mRNA was extracted from the liver.Ten clinical measurements of markers for liver injury are available for each subject.The microarray data contain expression levels of 3,116 genes. The data were normalised and preprocessed by Bushel, Wolfinger, and Gibson (2007).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46133) of an [OpenML dataset](https://www.openml.org/d/46133). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46133/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46133/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46133/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

